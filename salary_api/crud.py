from typing import Optional, Union
import bcrypt

from sqlalchemy.orm import Session

from . import models, schemas 

def get_employee(db: Session, employee_id_or_email: Union[int, str]):
    if isinstance(employee_id_or_email, int):
        return db.get(models.Employee, employee_id_or_email)
    return db.query(models.Employee).filter(models.Employee.email == employee_id_or_email).first()

def get_employees(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Employee).offset(skip).limit(limit).all()

def create_employee(db: Session, employee: schemas.EmployeeCreate):
    pw_bytes = employee.hashed_password.encode("utf-8")
    hashed_pw = bcrypt.hashpw(pw_bytes, bcrypt.gensalt())
    employee_dict = employee.dict(exclude={"hashed_password"})
    db_employee = models.Employee(**employee_dict, hashed_password=hashed_pw)
    db.add(db_employee)
    db.commit()
    db.refresh(db_employee)
    return db_employee

def delete_employee(db: Session, employee_id_or_email: Union[int, str]):
    if isinstance(employee_id_or_email, int):
        db_employee = db.get(models.Employee, employee_id_or_email)
    else:
        db_employee = db.query(models.Employee).filter(models.Employee.email == employee_id_or_email).first()
    
    if db_employee:
        db.delete(db_employee)
        db.commit()
        return True
    return False
        
def update_employee(db: Session, employee_new: schemas.EmployeeUpdate, employee: schemas.Employee):
    for field, value in employee_new.dict(exclude_unset=True).items():
        if field == "hashed_password":
            pw_bytes = value.encode("utf-8")
            hashed_pw = bcrypt.hashpw(pw_bytes, bcrypt.gensalt())
            db_employee = db.get(models.Employee, employee.id)
            db_employee.hashed_password = hashed_pw;
        else:
            setattr(employee, field, value)
        db.commit()
        db.refresh(employee)
    return employee 



def get_salary(db: Session, salary_id: int):
    return db.get(models.Salary, salary_id)

def get_salaries(db: Session, email: Optional[str] = None, skip: int = 0, limit: int = 100):
    if email:
        return db.query(models.Salary).filter(
                    models.Salary.employee_email == email
                ).offset(skip).limit(limit).all()
    return db.query(models.Salary).offset(skip).limit(limit).all()

def create_employee_salary(db: Session, salary: schemas.SalaryCreate):
    db_salary = models.Salary(**salary.dict())
    db.add(db_salary)
    db.commit()
    db.refresh(db_salary)
    return db_salary

def delete_salary(db: Session, salary_id: int):
    db_salary = db.get(models.Salary, salary_id)
    
    if db_salary:
        db.delete(db_salary)
        db.commit()
        return True
    return False

def update_salary(db: Session, salary_new: schemas.SalaryUpdate, salary: schemas.Salary):
    for field, value in salary_new.dict(exclude_unset=True).items():
        setattr(salary, field, value)
        db.commit()
        db.refresh(salary)
    return salary 

