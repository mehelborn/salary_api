from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Numeric, Date 

from .database import Base

class Employee(Base):
    __tablename__ = "employees"

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True, nullable=False)
    first_name = Column(String, index=True, nullable=False)
    last_name = Column(String, index=True, nullable=False)
    surname = Column(String, index=True, nullable=False)
    hashed_password = Column(String, nullable=False)
    is_active = Column(Boolean, default = True, nullable=False)
    role = Column(String, default="employee", nullable=False)

class Salary(Base):
    __tablename__ = "salaries"

    id = Column(Integer, primary_key=True, index=True)
    payday = Column(Date, index=True, nullable=False)
    amount = Column(Numeric(precision=15, scale=2), nullable=False)
    district_coefficient = Column(Numeric(precision=15, scale=2), nullable=False)
    personal_income_tax = Column(Numeric(precision=15, scale=2), nullable=False)
    payoff = Column(Numeric(precision=15, scale=2), nullable = False)
    salary_increase_date = Column(Date, nullable=False)
    employee_email = Column(String, ForeignKey("employees.email"), nullable=False)
