from typing import Optional
from decimal import Decimal
from pydantic import BaseModel 
from datetime import date

class EmployeeBase(BaseModel):
    first_name: Optional[str] = None
    last_name: Optional[str] = None
    surname: Optional[str] = None
    email: Optional[str] = None

class EmployeeCreate(EmployeeBase):
    hashed_password: str
    role: str

class Employee(EmployeeBase):
    id: int
    is_active: Optional[bool] = True 
    role: str

    class Config:
        orm_mode = True

class EmployeeUpdate(EmployeeBase):
    hashed_password: Optional[str] = None
    is_active: Optional[bool] = None
    role: Optional[str] = None



class SalaryBase(BaseModel):
    amount: Optional[Decimal] = None
    district_coefficient: Optional[Decimal] = None 
    personal_income_tax: Optional[Decimal] = None
    payoff: Optional[Decimal] = None
    salary_increase_date: Optional[date] = None 

class SalaryCreate(SalaryBase):
    payday: date
    employee_email : str 

class Salary(SalaryBase):
    id: int
    payday: date
    employee_email: str 

    class Config:
        orm_mode = True

class SalaryUpdate(SalaryBase):
    payday: Optional[date] = None
    employee_email : Optional[str] = None 



class Token(BaseModel):
    access_token: str
    token_type: str

class TokenData(BaseModel):
    username: str | None = None



Employee.update_forward_refs()
