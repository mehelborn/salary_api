from typing import Annotated, List, Optional, Union
from datetime import timedelta, datetime
from fastapi import FastAPI, Depends, HTTPException, Query, status
from sqlalchemy.orm import Session 
from dotenv import load_dotenv
from jose import JWTError, jwt 
from fastapi.security import OAuth2PasswordRequestForm, OAuth2PasswordBearer 
from passlib.context import CryptContext
from salary_api.schemas import TokenData
import os

from . import crud, models, schemas
from .database import SessionLocal, engine 

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

def get_db():
    db = SessionLocal()
    try: 
        yield db
    finally:
        db.close()

load_dotenv()

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)

def get_password_hash(password):
    return pwd_context.hash(password)

def authenticate_user(db, username: str, password: str):
    user_db = crud.get_employee(db, username)
    if not user_db:
        return False
    if not verify_password(password, user_db.hashed_password):
        return False
    return user_db

def create_access_token(data: dict, expires_delta: timedelta | None = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta 
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, os.environ.get("SECRET_KEY"), algorithm=os.environ.get("ALGORITHM"))
    return encoded_jwt

async def get_current_user(token: Annotated[str, Depends(oauth2_scheme)], db: Session = Depends(get_db)):
    credentials_exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, os.environ.get("SECRET_KEY"), 
                            algorithms=[os.environ.get("ALGORITHM")])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except JWTError:
        raise credentials_exception

    user = crud.get_employee(db, token_data.username)
    if user is None:
        raise credentials_exception
    return user

async def get_current_active_user(
    current_user: Annotated[models.Employee, Depends(get_current_user)]
):
    if not current_user.is_active:
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user

@app.post("/token", response_model=schemas.Token)
async def login_for_access_token(
    form_data: Annotated[OAuth2PasswordRequestForm, Depends()],
    db: Session = Depends(get_db)
):
    user = authenticate_user(db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=int(os.environ.get("ACCESS_TOKEN_EXPIRE_MINUTES")))
    access_token = create_access_token(
        data={"sub": user.email}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}



@app.post("/employees/", response_model=schemas.Employee)
def create_employee(current_user: Annotated[models.Employee, Depends(get_current_active_user)], 
                    employee: schemas.EmployeeCreate, 
                    db: Session = Depends(get_db)):

    if current_user.role == "admin":
        db_employee = crud.get_employee(db, employee.email) 
        if db_employee:
            raise HTTPException(status_code=400, detail={"message": "Email already registered"})
        return crud.create_employee(db=db, employee=employee)
    else:
        raise HTTPException(status_code=403, detail={"message": "You do not have access"})

@app.get("/employees/", response_model=List[schemas.Employee])
def read_employees(current_user: Annotated[models.Employee, Depends(get_current_active_user)], 
                   skip: int = 0, limit: int = 100, 
                   db: Session = Depends(get_db)):
    
    if current_user.role == "admin":
        return crud.get_employees(db, skip, limit)
    else:
        return [current_user]

@app.get("/employees/{id_or_email}/", response_model=schemas.Employee)
def read_employee(current_user: Annotated[models.Employee, Depends(get_current_active_user)], 
                  id_or_email: Union[int, str], 
                  db: Session = Depends(get_db)):

    if current_user.role == "admin":
        db_employee = crud.get_employee(db, employee_id_or_email=id_or_email)

        if db_employee is None:
            raise HTTPException(status_code=404, detail={"message": "Employee not found"})
        return db_employee
    else:
        return current_user

@app.delete("/employees/{id_or_email}/")
def delete_employee(current_user: Annotated[models.Employee, Depends(get_current_active_user)],
                    id_or_email: Union[int, str],
                    db: Session = Depends(get_db)):
    
    if current_user.role == "admin":
        deleted = crud.delete_employee(db, employee_id_or_email=id_or_email) 

        if deleted:
            return {"message": "Employee deleted"}
        raise HTTPException(status_code=500, detail={"message": "Employee not deleted!"})
    else:
        raise HTTPException(status_code=403, detail={"message": "You do not have access"})  
        
@app.put("/employees/{id_or_email}/", response_model=schemas.Employee)
def update_employee(current_user: Annotated[models.Employee, Depends(get_current_active_user)],
                    id_or_email: Union[int, str], 
                    employee: schemas.EmployeeUpdate, 
                    db: Session = Depends(get_db)):

    if current_user.role == "admin": 
        db_employee = crud.get_employee(db, id_or_email)

        if db_employee:
            return crud.update_employee(db, employee, db_employee)
        raise HTTPException(status_code=404, detail={"message": "Employee not found!"})
    else:
        raise HTTPException(status_code=403, detail={"message": "You do not have access"})  



@app.get("/salaries/{id}", response_model=schemas.Salary)
def read_salary(current_user: Annotated[models.Employee, Depends(get_current_active_user)],
                id: int,
                db: Session = Depends(get_db)):
    
    db_salary = crud.get_salary(db, id)
    if not db_salary:
        raise HTTPException(status_code=404, detail={"message": "Document not found"})
    if db_salary.employee_email != current_user.email and current_user.role != "admin":
        raise HTTPException(status_code=403, detail={"message": "You do not have access"})
    return db_salary

@app.get("/salaries/", response_model=List[schemas.Salary])
def read_salaries(current_user: Annotated[models.Employee, Depends(get_current_active_user)],
                  email: Optional[str] = Query(None), skip: int = 0,
                  limit: int = 100, db: Session = Depends(get_db)):

    if current_user.role == "admin":
        db_salaries = crud.get_salaries(db, email, skip, limit)
    else:
        db_salaries = crud.get_salaries(db, current_user.email, skip, limit)
    return db_salaries

@app.post("/salaries/", response_model=schemas.Salary)
def create_salary(current_user: Annotated[models.Employee, Depends(get_current_active_user)],
                  salary: schemas.SalaryCreate, db: Session = Depends(get_db)):

    if current_user.role == "admin":
        model_salary = models.Salary
        db_salary = db.query(models.Salary).filter(
            model_salary.payday == salary.payday,
            model_salary.employee_email == salary.employee_email
        ).first()

        if db_salary:
            raise HTTPException(status_code=400, detail={"message": "Document already exists for this date."})
        return crud.create_employee_salary(db, salary)
    else:
        raise HTTPException(status_code=403, detail={"message": "You do not have access"})
    
@app.delete("/salaries/{id}/")
def delete_salary(current_user: Annotated[models.Employee, Depends(get_current_active_user)], 
                   id: int, db: Session = Depends(get_db)):

    if current_user.role == "admin":
        deleted = crud.delete_salary(db, id)

        if deleted:
            return {"message": "Document deleted!"}
        raise HTTPException(status_code=500, detail={"message": "Document not deleted!"})
    else:
        raise HTTPException(status_code=403, detail={"message": "You do not have access"})


@app.put("/salaries/{id}/", response_model=schemas.Salary)
def update_salary(current_user: Annotated[models.Employee, Depends(get_current_active_user)],
                  id: int, salary: schemas.SalaryUpdate, db: Session = Depends(get_db)):

    if current_user.role == "admin":
        db_salary = crud.get_salary(db, id)

        if db_salary:
            return crud.update_salary(db, salary, db_salary)
        raise HTTPException(status_code=404, detail={"message": "Document not found!"})
    else:
        raise HTTPException(status_code=403, detail={"message": "You do not have access"})

