# Documentation

## Overview

This FastAPI app is designed to provide an API for managing employee salaries. It allows users to authenticate, create access tokens, and perform CRUD operations on employee salary data.

## Setup

To set up and run this FastAPI app, follow these steps:

1. Clone the repository: 

```shell
git clone https://gitlab.com/mehelborn/salary_api.git/
```

2. Install dependencies: 

```shell
poetry install
```

3. Activate the environment:

```shell
poetry shell
```

4. Start the server:

```shell
uvicorn salary_api.main:app
```

## Usage

### Authentication

To authenticate and obtain an access token, send a POST request to the `/token` endpoint with the following form data:

`username: mr.4uldum@gmail.com`

`password: keygen`

Example request:

```shell
  curl -X 'POST' \
  'http://127.0.0.1:8000/token' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -d 'grant_type=&username=your_email_address&password=your_password&scope=&client_id=&client_secret='
```

The response will contain the access token:

```json
{
  "access_token": "your_access_token",
  "token_type": "bearer"
}
```

Use this access token as a Bearer token in the Authorization header for subsequent requests to protected endpoints.

### Employee Management   

- Create Employee: To create a new employee, send a POST request to `/employees/` with the employee's information in the request body. Only admin users can create new employees.

Example request:

```shell
   curl -X 'POST' \
  'http://127.0.0.1:8000/employees/' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -H 'Authorization: Bearer your_access_token' \
  -d '{
        "first_name": "string",
        "last_name": "string",
        "surname": "string",
        "email": "string",
        "hashed_password": "string",
        "role": "string"
  }'
```

- Read Employees: To retrieve a list of employees, send a GET request to `/employees/`. Admin users will receive a list of all employees, while non-admin users will receive a list containing only their own information.

Example request:

```shell
    curl -X 'GET' \
    'http://127.0.0.1:8000/employees/?skip=0
    limit=100' \
    -H 'Authorization: Bearer your_access_token' \
    -H 'accept: application/json'
```

- Read Employee: To retrieve information about a specific employee, send a GET request to `/employees/{id_or_email}/`, where id_or_email can be either the employee's ID or email address. Admin users can access any employee's information, while non-admin users can only access their own information.

```shell
    curl -X 'GET' \
    'http://127.0.0.1:8000/employees/1/' \
    -H 'Authorization: Bearer your_access_token' \
    -H 'accept: application/json'
```

- Update Employee: To update an employee's information, send a PUT request to `/employees/{id_or_email}/` with the updated information in the request body. Only admin users can update employee information.

```shell
    curl -X 'PUT' \
    'http://127.0.0.1:8000/employees/1/' \
    -H 'accept: application/json' \
    -H 'Authorization: Bearer your_access_token' \
    -H 'Content-Type: application/json' \
    -d '{
        "first_name": "string",
        "last_name": "string",
        "surname": "string",
        "email": "string",
        "hashed_password": "string",
        "is_active": true,
        "role": "string"
    }'    
```

- Delete Employee: To delete an employee, send a DELETE request to `/employees/{id_or_email}/`. Only admin users can delete employee records.

```shell
    curl -X 'DELETE' \
    'http://127.0.0.1:8000/employees/%60/' \
    -H 'Authorization: Bearer your_access_token' \
    -H 'accept: application/json'
```

### Salary Management

- Create Salary: To create a new salary record, send a POST request to `/salaries/` with the salary information in the request body. Only admin users can create salary records.

```shell
    curl -X 'POST' \
    'http://127.0.0.1:8000/salaries/' \
    -H 'accept: application/json' \
    -H 'Authorization: Bearer your_access_token' \
    -H 'Content-Type: application/json' \
    -d '{
        "amount": 0,
        "district_coefficient": 0,
        "personal_income_tax": 0,
        "payoff": 0,
        "salary_increase_date": "2023-06-20",
        "payday": "2023-06-20",
        "employee_email": "string"
    }'
```

- Read Salaries: To retrieve a list of salary records, send a GET request to `/salaries/`. Admin users can access all salary records, while non-admin users can only access their own salary information.

```shell
    curl -X 'GET' \
    'http://127.0.0.1:8000/salaries/?email=mr.4uldum%40gmail.com&skip=0&limit=100' \
    -H 'Authorization: Bearer your_access_token' \
    -H 'accept: application/json'
```

- Read Salary: To retrieve information about a specific salary record, send a GET request to `/salaries/{id}/`. Admin users can access any salary record, while non-admin users can only access their own salary information.

```shell
    curl -X 'GET' \
    'http://127.0.0.1:8000/salaries/1' \
    -H 'Authorization: Bearer your_access_token' \
    -H 'accept: application/json'
```

- Update Salary: To update a salary record, send a PUT request to `/salaries/{id}/` with the updated information in the request body. Only admin users can update salary records.

```shell
    curl -X 'PUT' \
    'http://127.0.0.1:8000/salaries/1/' \
    -H 'accept: application/json' \
    -H 'Authorization: Bearer your_access_token' \
    -H 'Content-Type: application/json' \
    -d '{
        "amount": 0,
        "district_coefficient": 0,
        "personal_income_tax": 0,
        "payoff": 0,
        "salary_increase_date": "2023-06-20",
        "payday": "2023-06-20",
        "employee_email": "string"
    }'
```

- Delete Salary: To delete a salary record, send a DELETE request to `/salaries/{id}/`. Only admin users can delete salary records.

```shell
    curl -X 'DELETE' \
    'http://127.0.0.1:8000/salaries/1/' \
    -H 'Authorization: Bearer your_access_token' \
    -H 'accept: application/json'
```
